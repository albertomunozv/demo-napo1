# README #

This is a demo setup with Pulumi

### A GKE setup with Pulumi ###

* Demo setup with Pulumi of a GKE cluster with a helloworld deployment
* Version 1

### Setup ###

* Clone this repository from
	[demo napo](https://bitbucket.org/albertomunozv/demo-napo1)
Change to the directory of the repo and run all the commands from there.
    ```bash
    cd demo-napo1
    ```

* Install the Google cloud SDK and setup a account on GCP
  [GCP](https://cloud.google.com)
  [Install Google Cloud SDK (`gcloud`)](https://cloud.google.com/sdk/docs/downloads-interactive)
 
* Install the pulumi binary
  [Install Pullumi](https://www.pulumi.com/docs/get-started/install/)
	```bash
	curl -fsSL https://get.pulumi.com | sh
	```
Note: The first time pulumi runs, it will guide you to create an account and provide you with a token to authenticate the cli.

* Setup a service account to use in GCP: 
	[gcloud service account](https://www.pulumi.com/docs/intro/cloud-providers/gcp/service-account/)
	Download the credentials.json file with the credentials and store them in an environment variable
	```bash
	export GOOGLE_CREDENTIALS=$(cat credentials.json)
	```
	From now on, the credentials of the GCP account will be taken from the environment variable.

*  [Install Node.js](https://nodejs.org/en/download/)

  Install the required dependencies.
      ```bash
    npm install
    ```
   
* Create a new Pulumi stack
    ```bash
    pulumi stack init
    ```

* Set the GCP configuration variables for your environment

    ```bash
    # list the GCP projects available or create a new one
    gcloud projects list
    # choose from the list
    pulumi config set gcp:project <project>
    # get a list of GCP zones to use
    gcloud compute zones list
    #choose from the previous list
    pulumi config set gcp:zone <zone>
    ```
* Stand up the cluster

    ```bash
    pulumi up
    ```
This will take 7-8 minutes in GCP to complete.

* Install kubectl and use it to access the k8s cluster
  Pulumi can help create a kubeconfig file to be used by kubectl
    ```bash
    pulumi stack output kubeconfig --show-secrets > kubeconfig
    export KUBECONFIG=$PWD/kubeconfig
    export KUBERNETES_VERSION=1.11.6 && sudo curl -s -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl && sudo chmod +x /usr/local/bin/kubectl

    kubectl version
    kubectl cluster-info
    kubectl get nodes
    ```
  Use the output from Pulumi to obtain information from the cluster
    ```bash
    kubectl get deployment $(pulumi stack output deploymentName) --namespace=$(pulumi stack output namespaceName)
    kubectl get service $(pulumi stack output serviceName) --namespace=$(pulumi stack output namespaceName)
    ```
Note the public ip of the service and open a browser with it, like http://<service ip>

* Destroy the cluster and all associated resources
  Once the demo is complete, you can destroy all the resources
    ```bash
    pulumi destroy --yes
    pulumi stack rm --yes
    ```


### Who to contact ###

* Alberto Munoz
